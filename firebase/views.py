from django.shortcuts import render, redirect
import pyrebase
from django.http import HttpResponse
from .models import  CustomUser
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout


config = {
    "apiKey": "AIzaSyBt8JZRwPtMJl3HAiPNwLmRvY3Ra1k3Y38",
    "authDomain": "zeon-store-70d53.firebaseapp.com",
    "databaseURL": "https://zeon-store-70d53-default-rtdb.firebaseio.com/",
    "projectId": "zeon-store-70d53",
    "storageBucket": "zeon-store-70d53.appspot.com",
    "messagingSenderId": "292384377728",
    "appId": "1:292384377728:web:c7aab7fe302483cb3f8f65",
    "measurementId": "G-FDJFKX2Y4S"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
database = firebase.database()


def signIn(request):
    return render(request, "firebase/login.html")


def home(request):
    return render(request, "firebase/home.html")


def postsignIn(request):
    username = request.POST.get('username')
    email = request.POST.get('email')
    pasw = request.POST.get('pass')
    try:
        user_firebase = auth.sign_in_with_email_and_password(email, pasw)
        user_django = authenticate(request, username=username, password=pasw)
        if user_django is not None:
            login(request, user_django)
    except:
        message = "Invalid Credentials!!Please ChecK your Data"
        return render(request, "firebase/login.html", {"message": message})
    session_id = user_firebase['idToken']
    request.session['uid'] = str(session_id)
    return redirect("http://127.0.0.1:8000/swagger/")


def logout_firebase(request):
    try:
        if request.user.is_authenticated:
            print("=" * 10)
            logout(request)
            print("Django logout")
        else:
            print("=" * 10)
            print("You're already logged out from DJANGO, bitch")
        print("+"*10)
        del request.session['uid']
        print("Firebase logout")

    except:
        html = "<html><body><h1>You're already logged out from FIREBASE, bitch</h1></body></html>"
        return HttpResponse(html)
    return render(request, "firebase/login.html")


def signUp(request):
    return render(request, "firebase/registration.html")


def postsignUp(request):
    email = request.POST.get('email')
    passs = request.POST.get('pass')
    username = request.POST.get('username')
    try:
        # creating a user with the given email and password
        print(username, email, passs)
        new_user = CustomUser.objects.create_user(username=username, email=email, password=passs)
        user = auth.create_user_with_email_and_password(email, passs)



    except:
        html = "<html><body><h1>Failed to register, bitch</h1></body></html>"
        return HttpResponse(html)
    return render(request, "firebase/login.html")

from django.db import models
from django_countries.fields import CountryField
import datetime
from django.contrib.auth.models import AbstractUser
# Create your models here.


order_status_choices = [('new', 'Новый'),
                        ('ordered', 'Оформлен'),
                        ('canceled', 'Отменен')]


class CustomUser(AbstractUser):
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    city = models.CharField(max_length=20)
    order_date = models.DateField(default=datetime.date.today)
    order_status = models.CharField(choices=order_status_choices,
                                    default='new', max_length=10)

    # order = models.ForeignKey(Order, models.SET_NULL, blank=True, null=True,)
    def __str__(self):
        return str(self.first_name + " " + self.last_name)

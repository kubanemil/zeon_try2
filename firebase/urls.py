from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.signIn),
    path('postlogin/', views.postsignIn),
    path('signup/', views.signUp, name="signup"),
    path('logout/', views.logout_firebase, name="logout"),
    path('postsignup/', views.postsignUp, name='postsign'),
]

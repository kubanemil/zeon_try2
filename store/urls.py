"""urls for store app"""
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from . import views

urlpatterns = [
                  path("", views.home, name='home'),
                  path("api/collection", views.CollectionApi.as_view(), name='collection_api'),
                  path("api/item", views.ItemApi.as_view(), name='item_api'),
                  path("api/news", views.NewsApi.as_view(), name='news_api'),
                  path("api/about", views.AboutApi.as_view(), name='about_api'),
                  path("help", views.HelpApi.as_view(), name='help_api'),
                  path("footer", views.FooterApi.as_view(), name='footer_api'),
                  path("footerinfo", views.FooterInfoApi.as_view(), name='footerinfo_api'),
                  path("publicoffer", views.PublicOfferApi.as_view(), name='publicoffer_api'),
                  path("button", views.ButtonApi.as_view(), name="button_api"),
                  path("", include('rest_framework.urls')),
                  path("collection/create", views.CreateCollectionApi.as_view(), name='collection_create_api'),
                  path("favorite/add_remove/<pk>", views.AddDeleteFavoriteApi.as_view()),
                  path("favorite", views.FavoriteApi.as_view()),
                  path("cart/add_remove/<pk>", views.AddDeleteShoppingCartApi.as_view()),
                  path("cart", views.ShoppingCartApi.as_view()),
                  path("order", views.OrderApi.as_view()),
                  path("collection/<pk>/item", views.CollectionItemApi.as_view()),
                  path("item/search", views.SearchItemApi.as_view()),
                  path("item/random", views.RandomItemApi.as_view()),
                  path("item/new", views.NewItemApi.as_view()),
                  path("item/<pk>/detailed", views.DetailedItemApi.as_view()),
                  path("item/popular", views.PopularItemApi.as_view()),
                  path("appeals", views.AppealsApi.as_view()),
                  path("appeals/create", views.CreateAppealsApi.as_view()),
                  path("advantages", views.FourAdvantagesApi.as_view()),
                  path("slider", views.SliderApi.as_view()),
                  path("customer", views.CustomerApi.as_view()),
                  path("customer/create", views.CreateCustomerApi.as_view()),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

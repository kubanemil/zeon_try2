"""File with Pagination classes"""
from rest_framework.pagination import PageNumberPagination


class MainPagination(PageNumberPagination):
    """Main pagination for Collection API"""
    page_size = 8
    max_page_size= 10


class FourPagination(PageNumberPagination):
    """Four page pagination"""
    page_size = 4
    max_page_size = 10


class FivePagination(PageNumberPagination):
    """Five page pagination"""
    page_size = 5
    max_page_size = 10


class TwelvePagination(PageNumberPagination):
    """Twelve page pagination"""
    page_size = 12
    max_page_size = 20

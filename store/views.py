"""API Views for each model"""
from django.shortcuts import render
from rest_framework.generics import ListAPIView, \
    RetrieveAPIView, CreateAPIView, RetrieveUpdateAPIView
from rest_framework import filters
from drf_multiple_model.views import ObjectMultipleModelAPIView
from .service import MainPagination, FivePagination, FourPagination, TwelvePagination
from .serializer import *


def home(request):
    """Home view"""
    return render(request, "store/home.html", context={'username': request.user})


class OrderApi(ObjectMultipleModelAPIView):
    """GET API for Orders"""
    querylist = [
        {
            'queryset': Item.objects.filter(in_cart=True),
            'serializer_class': OrderSerializer,
            'label': 'Payment Info'
        },
        {
            'queryset': Item.objects.filter(in_cart=True),
            'serializer_class': FavoriteSerializer,
            'label': 'Cart Item'
        },
    ]


class CustomerApi(ListAPIView):
    """GET API for Collection"""
    queryset = Customer.objects.all()
    serializer_class = UserSerializer


class CreateCustomerApi(CreateAPIView):
    """ GET API for Collection"""
    serializer_class = UserSerializer


class CollectionApi(ListAPIView):
    """GET API for Collection"""
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    pagination_class = MainPagination


class CreateCollectionApi(CreateAPIView):
    """POST API for Collection"""
    serializer_class = CollectionSerializer


class ItemApi(ListAPIView):
    """GET API for Item"""
    queryset = Item.objects.all()
    serializer_class = ItemSerializer


class SearchItemApi(ListAPIView):
    """Search API for Item"""
    queryset = Item.objects.all()
    serializer_class = FavoriteSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', ]
    pagination_class = FivePagination


class RandomItemApi(ListAPIView):
    """GET API that returns Items from each section"""
    queryset = Item.item_from_each_collection
    serializer_class = FavoriteSerializer
    pagination_class = FivePagination


class PopularItemApi(ListAPIView):
    """GET API that returns popular Items """
    queryset = Item.objects.filter(is_popular=True)
    serializer_class = FavoriteSerializer
    pagination_class = MainPagination


class NewItemApi(ListAPIView):
    """GET API for New Items"""
    queryset = Item.objects.filter(is_new=True)
    serializer_class = FavoriteSerializer
    pagination_class = MainPagination


class DetailedItemApi(RetrieveAPIView):
    """GET API that returns Detailed info
    About Item instance and additionally returns
    few random Items """
    queryset = Item.objects.all()
    serializer_class = DetailedItemSerializer


class AddDeleteFavoriteApi(RetrieveUpdateAPIView):
    """PUT/PATCH API for Favorite Items"""
    queryset = Item.objects.all()
    serializer_class = FavoriteSerializer


class FavoriteApi(ListAPIView):
    """ GET API for Favorite Items """
    queryset = Item.objects.filter(is_favorite=True)
    serializer_class = FavoriteSerializer
    pagination_class = TwelvePagination


class NewsApi(ListAPIView):
    """ GET API for News """
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    pagination_class = MainPagination


class AboutApi(ListAPIView):
    """GET API for About US section"""
    queryset = About.objects.all()
    serializer_class = AboutSerializer


class HelpApi(ObjectMultipleModelAPIView):
    """ GET API made from
    HelpPhoto model and Help model."""

    querylist = [

        {
            'queryset': HelpPhoto.objects,
            'serializer_class': HelpPhotoSerializer,
            'label': 'photo'
        },
        {
            'queryset': Help.objects.all(),
            'serializer_class': HelpSerializer,
            'label': 'help'
        }
    ]


class FooterApi(ListAPIView):
    """ GET API for Footer """
    queryset = Footer.objects.all()
    serializer_class = FooterSerializer


class FooterInfoApi(ListAPIView):
    """GET API for Footer Information"""
    queryset = FooterInfo.objects.all()
    serializer_class = FooterInfoSerializer


class PublicOfferApi(ListAPIView):
    """GET API for Public Offer"""
    queryset = PublicOffer.objects.all()
    serializer_class = PublicOfferSerializer


class CollectionItemApi(RetrieveAPIView):
    """GET API for Item of the Collection"""
    queryset = Collection.objects.all()
    serializer_class = CollectionItemSerializer
    pagination_class = TwelvePagination


class ShoppingCartApi(ListAPIView):
    """GET API for Shopping Cart Items"""
    queryset = Item.objects.filter(in_cart=True)
    serializer_class = ShoppingCartSerialzier


class AddDeleteShoppingCartApi(RetrieveUpdateAPIView):
    """PUT/PATCH API for Shopping Cart Items"""
    queryset = Item.objects.all()
    serializer_class = ShoppingCartSerialzier


class ButtonApi(ListAPIView):
    """GET API for Button"""
    queryset = Button.objects.all()
    serializer_class = ButtonSerializer


class AppealsApi(ListAPIView):
    """GET API for Appeals"""
    queryset = Appeals.objects.all()
    serializer_class = AppealsSerializer


class CreateAppealsApi(CreateAPIView):
    """POST API for Appeals"""
    serializer_class = AppealsSerializer


class FourNewItemApi(ListAPIView):
    """GET API for New Items"""
    queryset = Item.objects.filter(is_new=True)
    serializer_class = FavoriteSerializer
    pagination_class = FourPagination


class FourAdvantagesApi(ListAPIView):
    """ GET API for Advantages """
    queryset = Advantages.objects.all()
    serializer_class = AdvantagesSerializer
    pagination_class = FourPagination


class SliderApi(ListAPIView):
    """ GET API for Slider """
    queryset = Slider.objects.all()
    serializer_class = SliderSerializer

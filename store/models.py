"""Django Models file to manage data"""
import datetime
from django.db import models
from ckeditor.fields import RichTextField
from colorfield.fields import ColorField
from solo.models import SingletonModel
from django_countries.fields import CountryField


class PhotoColor(models.Model):
    """Model to connect Photo with it's color."""

    class Meta:
        verbose_name = 'ФотоЦвет'
        verbose_name_plural = 'ФотоЦвета'

    photo = models.ImageField(upload_to='static/images/item',
                              default=r'static\images\image_11_1.png')
    color = ColorField()

    def __str__(self):
        return str(self.photo) + " " + str(self.color)


class Collection(models.Model):
    """Collection model"""

    class Meta:
        verbose_name = 'Коллекция'
        verbose_name_plural = 'Коллекции'

    photo1 = models.ImageField(upload_to='static/images/collection',
                               default=r'static\images\image_11_1.png')
    name = models.CharField(max_length=100, default="Unknown Collection")

    def __str__(self):
        return str(self.name)


class Item(models.Model):
    """Model for collection item"""

    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'

    name = models.CharField(max_length=100)
    articule = models.CharField(max_length=200)
    photocolor1 = models.ForeignKey(PhotoColor, related_name='photocolor1',
                                    on_delete=models.CASCADE)
    photocolor2 = models.ForeignKey(PhotoColor, related_name='photocolor2',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor3 = models.ForeignKey(PhotoColor, related_name='photocolor3',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor4 = models.ForeignKey(PhotoColor, related_name='photocolor4',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor5 = models.ForeignKey(PhotoColor, related_name='photocolor5',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor6 = models.ForeignKey(PhotoColor, related_name='photocolor6',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor7 = models.ForeignKey(PhotoColor, related_name='photocolor7',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    photocolor8 = models.ForeignKey(PhotoColor, related_name='photocolor8',
                                    on_delete=models.CASCADE,
                                    null=True, blank=True)
    collection = models.ForeignKey(Collection, related_name="items",
                                   on_delete=models.CASCADE)
    price = models.IntegerField(default=1)
    old_price = models.IntegerField(default=1)
    description = RichTextField()  # ckeditor
    size = models.CharField(max_length=10, default="30-45")
    tissue_components = models.CharField(max_length=1000, default='cloth')
    amount = models.IntegerField(default=5)
    num_of_arrays = models.IntegerField(default=1)
    material = models.CharField(max_length=1000, default='cloth')
    is_popular = models.BooleanField(default=False)
    is_new = models.BooleanField(default=True)
    is_favorite = models.BooleanField(default=False)
    in_cart = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)

    @property
    def discount(self):
        """Calculates discount for the item instance"""
        return self.old_price - self.price

    @property
    def shopping_cart_total_old(self):
        """Sums old_price field of all in-cart items"""
        total_sum = 0
        for cart_item in Item.objects.filter(in_cart=True):
            total_sum += cart_item.old_price
        return total_sum

    @property
    def array_amount(self):
        """Sums the array amount of all in-cart items"""
        amount_of_array = 0
        for cart_item in Item.objects.filter(in_cart=True):
            amount_of_array += cart_item.num_of_arrays
        return amount_of_array

    @property
    def item_amount(self):
        """Total amount of item in the cart"""
        self.array_amount * self.amount

    @property
    def total_discount(self):
        """Total discount for all in-cart items"""
        return self.shopping_cart_total_old - self.shopping_cart_total

    @property
    def shopping_cart_total(self):
        """Total price for the purchase, considering discount"""
        total_sum = 0
        for cart_item in Item.objects.filter(in_cart=True):
            total_sum += cart_item.price
        return total_sum

    @property
    def item_from_each_collection(self):
        """Returns one item from each collection, if any."""
        items = []

        class ListAsQuerySet(list):
            """Class to convert list of objects into queryset"""

            def __init__(self, *args, model, **kwargs):
                self.model = model
                super().__init__(*args, **kwargs)

            def filter(self, *args, **kwargs):
                return self

            def order_by(self, *args, **kwargs):
                return self

        for col in Collection.objects.all():
            first_item = Item.objects.filter(collection=col)
            if len(first_item) >= 1:
                items.append(first_item[0])
        queryset = ListAsQuerySet(items, model=Item)
        return queryset


class News(models.Model):
    """News model"""

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'

    photo = models.ImageField(upload_to='static/images/news')
    title = models.CharField(max_length=50)
    description = RichTextField()  # ckeditor

    def __str__(self):
        return str(self.title)


class About(SingletonModel):
    """Model for `About Us` page"""

    class Meta:
        verbose_name = 'О нас'
        verbose_name_plural = 'О нас'

    photo1 = models.FileField(upload_to='static/images/about',
                              default=r'static\images\image_11_1.png')
    photo2 = models.FileField(upload_to='static/images/about', null=True, blank=True)
    photo3 = models.FileField(upload_to='static/images/about', null=True, blank=True)
    title = models.CharField(max_length=50)
    description = RichTextField()  # ckeditor

    def __str__(self):
        return str(self.title)


class HelpPhoto(SingletonModel):
    """Model that contains single Photo for `Help` Section"""

    class Meta:
        verbose_name = 'Фото для Помощь'
        verbose_name_plural = 'Фото для Помощь'

    photo = models.ImageField()

    def __str__(self):
        return str(self.photo)


class Help(models.Model):
    """`FAQ` section page"""

    class Meta:
        verbose_name = 'Помощь'
        verbose_name_plural = 'Помощи'

    question = models.CharField(max_length=500)
    answer = models.TextField()

    def __str__(self):
        return str(self.question)


class Footer(SingletonModel):
    """Singleton model for footer object"""

    class Meta:
        verbose_name = 'Футер'
        verbose_name_plural = 'Футер'

    logotype = models.FileField(upload_to='static/icons/footer')
    info = models.TextField()
    number = models.CharField(max_length=100)


class Advantages(models.Model):
    """ `Our Advantages!` model """

    class Meta:
        verbose_name = 'Премущество'
        verbose_name_plural = 'Наши премущества'

    icon = models.FileField(upload_to='static/icons/advantages')
    title = models.CharField(max_length=100)
    description = models.TextField()

    def __str__(self):
        return str(self.title)


class Slider(models.Model):
    """Slider model for main page"""

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдеры'

    photo = models.ImageField(upload_to="static/images/slider")
    link = models.CharField(max_length=1000, null=True, blank=True)


class PublicOffer(SingletonModel):
    """Public Offer to suggest, while creating an Purchase instance"""

    class Meta:
        verbose_name = 'Публичная Офферта'
        verbose_name_plural = 'Публичная Офферта'

    title = models.CharField(max_length=100)
    description = RichTextField()  # ckeditor

    def __str__(self):
        return str(self.title)


contact_choice = [('phone', 'Phone Number'), ('email', 'E-mail'),
                  ('instagram', 'Instagram'), ('telegram', 'Telegram'), ('whatsapp', 'Whatsapp')]


class FooterInfo(SingletonModel):
    """Information for footer of the website."""

    class Meta:
        verbose_name = 'Футер Инфо'
        verbose_name_plural = 'Футер Инфо'

    contact_type = models.CharField(choices=contact_choice, max_length=100)
    link = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        """Modified save() function to
        convert number to Whatsapp link"""

        if self.contact_type == 'whatsapp':
            if 'https://wa.me/' not in str(self.link):
                self.link = 'https://wa.me/' + self.link
        elif self.contact_type == 'phone':
            self.link = self.link
        super(FooterInfo, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.link)


class Button(SingletonModel):
    """Contact button with phone number data"""

    class Meta:
        verbose_name = 'Плавающая кнопка'
        verbose_name_plural = 'Плавающая кнопка'

    whatsapp_number = models.CharField(max_length=100)
    telegram_link = models.CharField(max_length=100)

    def save(self, *args, **kwargs):
        """o convert number to whatsapp link"""
        if 'https://wa.me/' not in str(self.whatsapp_number):
            self.whatsapp_number = 'https://wa.me/' + self.whatsapp_number
        super(Button, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.telegram_link)


yes_no = [('Yes', 'yes'), ('No', 'no')]


class Appeals(models.Model):
    """Application from user model"""

    class Meta:
        verbose_name = 'Заявка'
        verbose_name_plural = 'Заявки'

    name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=15)
    date = models.DateField(default=datetime.date.today)
    request_type = models.CharField(max_length=10)
    have_called = models.CharField(default='No', choices=yes_no, max_length=3)


order_status_choices = [('new', 'Новый'),
                        ('ordered', 'Оформлен'),
                        ('canceled', 'Отменен')]


class Customer(models.Model):
    """Customer Information model for arranging order"""

    class Meta:
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    email = models.EmailField()
    phone_number = models.CharField(max_length=15, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    city = models.CharField(max_length=20)
    order_date = models.DateField(default=datetime.date.today)
    order_status = models.CharField(choices=order_status_choices,
                                    default='new', max_length=10)

    def __str__(self):
        return str(self.first_name + " " + self.last_name)

# Generated by Django 4.0.4 on 2022-05-04 02:36

import ckeditor.fields
import colorfield.fields
import datetime
from django.db import migrations, models
import django.db.models.deletion
import django_countries.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo1', models.FileField(default='static\\images\\image_11_1.png', upload_to='static/images/about')),
                ('photo2', models.FileField(blank=True, null=True, upload_to='static/images/about')),
                ('photo3', models.FileField(blank=True, null=True, upload_to='static/images/about')),
                ('title', models.CharField(max_length=50)),
                ('description', ckeditor.fields.RichTextField()),
            ],
            options={
                'verbose_name': 'О нас',
                'verbose_name_plural': 'О нас',
            },
        ),
        migrations.CreateModel(
            name='Advantages',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('icon', models.FileField(upload_to='static/icons/advantages')),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
            ],
            options={
                'verbose_name': 'Премущество',
                'verbose_name_plural': 'Наши премущества',
            },
        ),
        migrations.CreateModel(
            name='Appeals',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('phone_number', models.CharField(max_length=15)),
                ('date', models.DateField(default=datetime.date.today)),
                ('request_type', models.CharField(max_length=10)),
                ('have_called', models.CharField(choices=[('Yes', 'yes'), ('No', 'no')], default='No', max_length=3)),
            ],
            options={
                'verbose_name': 'Заявка',
                'verbose_name_plural': 'Заявки',
            },
        ),
        migrations.CreateModel(
            name='Button',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('whatsapp_number', models.CharField(max_length=100)),
                ('telegram_link', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Плавающая кнопка',
                'verbose_name_plural': 'Плавающая кнопка',
            },
        ),
        migrations.CreateModel(
            name='Collection',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo1', models.ImageField(default='static\\images\\image_11_1.png', upload_to='static/images/collection')),
                ('name', models.CharField(default='Unknown Collection', max_length=100)),
            ],
            options={
                'verbose_name': 'Коллекция',
                'verbose_name_plural': 'Коллекции',
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=20)),
                ('last_name', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=254)),
                ('phone_number', models.CharField(blank=True, max_length=15, null=True)),
                ('country', django_countries.fields.CountryField(blank=True, max_length=2, null=True)),
                ('city', models.CharField(max_length=20)),
                ('order_date', models.DateField(default=datetime.date.today)),
                ('order_status', models.CharField(choices=[('new', 'Новый'), ('ordered', 'Оформлен'), ('canceled', 'Отменен')], default='new', max_length=10)),
            ],
            options={
                'verbose_name': 'Заказчик',
                'verbose_name_plural': 'Заказчики',
            },
        ),
        migrations.CreateModel(
            name='Footer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('logotype', models.FileField(upload_to='static/icons/footer')),
                ('info', models.TextField()),
                ('number', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Футер',
                'verbose_name_plural': 'Футер',
            },
        ),
        migrations.CreateModel(
            name='FooterInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('contact_type', models.CharField(choices=[('phone', 'Phone Number'), ('email', 'E-mail'), ('instagram', 'Instagram'), ('telegram', 'Telegram'), ('whatsapp', 'Whatsapp')], max_length=100)),
                ('link', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name': 'Футер Инфо',
                'verbose_name_plural': 'Футер Инфо',
            },
        ),
        migrations.CreateModel(
            name='Help',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('question', models.CharField(max_length=500)),
                ('answer', models.TextField()),
            ],
            options={
                'verbose_name': 'Помощь',
                'verbose_name_plural': 'Помощи',
            },
        ),
        migrations.CreateModel(
            name='HelpPhoto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='')),
            ],
            options={
                'verbose_name': 'Фото для Помощь',
                'verbose_name_plural': 'Фото для Помощь',
            },
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='static/images/news')),
                ('title', models.CharField(max_length=50)),
                ('description', ckeditor.fields.RichTextField()),
            ],
            options={
                'verbose_name': 'Новость',
                'verbose_name_plural': 'Новости',
            },
        ),
        migrations.CreateModel(
            name='PhotoColor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(default='static\\images\\image_11_1.png', upload_to='static/images/item')),
                ('color', colorfield.fields.ColorField(default='#FFFFFF', image_field=None, max_length=18, samples=None)),
            ],
            options={
                'verbose_name': 'ФотоЦвет',
                'verbose_name_plural': 'ФотоЦвета',
            },
        ),
        migrations.CreateModel(
            name='PublicOffer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('description', ckeditor.fields.RichTextField()),
            ],
            options={
                'verbose_name': 'Публичная Офферта',
                'verbose_name_plural': 'Публичная Офферта',
            },
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='static/images/slider')),
                ('link', models.CharField(blank=True, max_length=1000, null=True)),
            ],
            options={
                'verbose_name': 'Слайдер',
                'verbose_name_plural': 'Слайдеры',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('articule', models.CharField(max_length=200)),
                ('price', models.IntegerField(default=1)),
                ('old_price', models.IntegerField(default=1)),
                ('description', ckeditor.fields.RichTextField()),
                ('size', models.CharField(default='30-45', max_length=10)),
                ('tissue_components', models.CharField(default='cloth', max_length=1000)),
                ('amount', models.IntegerField(default=5)),
                ('num_of_arrays', models.IntegerField(default=1)),
                ('material', models.CharField(default='cloth', max_length=1000)),
                ('is_popular', models.BooleanField(default=False)),
                ('is_new', models.BooleanField(default=True)),
                ('is_favorite', models.BooleanField(default=False)),
                ('in_cart', models.BooleanField(default=False)),
                ('collection', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='store.collection')),
                ('photocolor1', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photocolor1', to='store.photocolor')),
                ('photocolor2', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor2', to='store.photocolor')),
                ('photocolor3', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor3', to='store.photocolor')),
                ('photocolor4', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor4', to='store.photocolor')),
                ('photocolor5', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor5', to='store.photocolor')),
                ('photocolor6', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor6', to='store.photocolor')),
                ('photocolor7', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor7', to='store.photocolor')),
                ('photocolor8', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='photocolor8', to='store.photocolor')),
            ],
            options={
                'verbose_name': 'Товар',
                'verbose_name_plural': 'Товары',
            },
        ),
    ]

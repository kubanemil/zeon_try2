"""API serializers"""
from rest_framework import serializers
from .models import Customer, Item, Collection, News, \
    Help, Advantages, Slider, Appeals, Button, About, \
    HelpPhoto, PublicOffer, Footer, FooterInfo


class UserSerializer(serializers.ModelSerializer):
    """User Serializer"""

    class Meta:
        """Meta class"""
        model = Customer
        fields = '__all__'


class CollectionSerializer(serializers.ModelSerializer):
    """Collection Serializer"""

    class Meta:
        """Meta class"""
        model = Collection
        fields = '__all__'


class ItemSerializer(serializers.ModelSerializer):
    """Item Serializer"""

    class Meta:
        """Meta class"""
        model = Item
        fields = '__all__'


class FavoriteSerializer(serializers.ModelSerializer):
    """Favorite Serializer"""

    class Meta:
        """Meta class"""
        model = Item
        fields = ('photocolor1', 'name', 'price', 'old_price',
                  'discount', 'size', 'id', 'is_favorite')

        read_only_fields = ('photocolor1', 'name', 'price',
                            'old_price', 'min_size', 'max_size', 'id',)


class ShoppingCartSerialzier(serializers.ModelSerializer):
    """Shopping Cart Serializer"""

    class Meta:
        """Meta class"""
        model = Item
        fields = ('id', 'photocolor1', 'name', 'price',
                  'old_price', 'size', 'in_cart', 'amount')

        read_only_fields = ('id', 'photocolor1', 'name',
                            'price', 'old_price', 'size',)


class OrderSerializer(serializers.ModelSerializer):
    """User Serializer"""

    class Meta:
        """Meta class"""
        model = Item
        fields = ('array_amount', 'item_amount',
                  'shopping_cart_total_old',
                  'total_discount', 'shopping_cart_total')


class CollectionItemSerializer(serializers.ModelSerializer):
    """The name of `items` variable must match
    with `related_name` in ForeignKey of Items"""
    items = FavoriteSerializer(many=True)

    class Meta:
        """Meta class"""
        model = Collection
        fields = ('id', 'name', 'items')


class DetailedItemSerializer(serializers.ModelSerializer):
    """Serializer that shows detailed information.
    Also includes additional random items from each collection"""
    item_from_each_collection = FavoriteSerializer(many=True)

    class Meta:
        """Meta class"""
        model = Item
        fields = ('collection', 'photocolor1', 'name', 'articule', 'price', 'old_price',
                  'description', 'size', 'tissue_components', 'num_of_arrays',
                  'material', 'is_popular', 'is_new', 'item_from_each_collection')


class NewsSerializer(serializers.ModelSerializer):
    """News Serializer"""

    class Meta:
        """Meta class"""
        model = News
        fields = '__all__'


class AboutSerializer(serializers.ModelSerializer):
    """About Serializer"""

    class Meta:
        """Meta class"""
        model = About
        fields = '__all__'


class HelpSerializer(serializers.ModelSerializer):
    """Help Serializer"""

    class Meta:
        """Meta class"""
        model = Help
        fields = '__all__'


class HelpPhotoSerializer(serializers.ModelSerializer):
    """Help Photo Serializer"""

    class Meta:
        """Meta class"""
        model = HelpPhoto
        fields = ('photo',)


class FooterSerializer(serializers.ModelSerializer):
    """Footer Serializer"""

    class Meta:
        """Meta class"""
        model = Footer
        fields = '__all__'


class FooterInfoSerializer(serializers.ModelSerializer):
    """Footer Information Serializer"""

    class Meta:
        """Meta class"""
        model = FooterInfo
        fields = '__all__'


class PublicOfferSerializer(serializers.ModelSerializer):
    """Public Offer Serializer"""

    class Meta:
        """Meta class"""
        model = PublicOffer
        fields = '__all__'


class ButtonSerializer(serializers.ModelSerializer):
    """Button Serializer"""

    class Meta:
        """Meta class"""
        model = Button
        fields = '__all__'


class AppealsSerializer(serializers.ModelSerializer):
    """Appeals Serializer"""

    class Meta:
        """Meta class"""
        model = Appeals
        fields = '__all__'


class AdvantagesSerializer(serializers.ModelSerializer):
    """Advantages Serializer"""

    class Meta:
        """Meta class"""
        model = Advantages
        fields = '__all__'


class SliderSerializer(serializers.ModelSerializer):
    """Slider Serializer"""

    class Meta:
        """Meta class"""
        model = Slider
        fields = '__all__'

"""Permission file to exclude third-party faces
from interrupting data """
from rest_framework import permissions


class IsReaderOnly(permissions.BasePermission):
    """Permission for Admin page"""

    def has_object_permission(self, request, view, obj):
        """Function to check permission eligibility"""
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.user == request.user

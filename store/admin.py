""" Registration of models in admin page."""
from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import Customer, Item, Collection, News, Help, \
    Advantages, Slider, PhotoColor, Appeals, Button, About, \
    HelpPhoto, PublicOffer, Footer, FooterInfo

admin.site.register(Customer)
admin.site.register(Item)
admin.site.register(Collection)
admin.site.register(News)
admin.site.register(Help)
admin.site.register(Advantages)
admin.site.register(Slider)
admin.site.register(PhotoColor)
admin.site.register(Appeals)
admin.site.register(Button, SingletonModelAdmin)
admin.site.register(About, SingletonModelAdmin)
admin.site.register(HelpPhoto, SingletonModelAdmin)
admin.site.register(PublicOffer, SingletonModelAdmin)
admin.site.register(Footer, SingletonModelAdmin)
admin.site.register(FooterInfo, SingletonModelAdmin)
